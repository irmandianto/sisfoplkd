<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'TampilanDepanController@home');

Route::get('/profile', 'TampilanDepanController@profile');
Route::get('/events', 'TampilanDepanController@event');
Route::get('/home-front', 'TampilanDepanController@home');
Route::get('/berita-home', 'TampilanDepanController@berita');
Route::get('/berita-detail/{id}/show', 'TampilanDepanController@beritadetail');

Auth::routes();

/*Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');*/

Route::post('/registrasiPeserta', 'RegistrasiPesertaController@registrasiPeserta');
Route::post('/registrasiDosen', 'RegistrasiPesertaController@registrasiDosen');
Route::post('/registrasiLain', 'RegistrasiPesertaController@registrasiLain');

Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('logout', '\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'home',  'middleware' => 'admin'], function () {
#crud profile
    Route::get('/profile/admin', 'AdminProfileController@index');
    Route::get('/profile/admin/{id}/edit', 'AdminProfileController@edit');
    Route::put('/profile/admin/{id}', 'AdminProfileController@update');
#end
#crud password
    Route::post('/password/admin/ubahpassword', 'AdminProfileController@ubahpassword');
#end
#crud fakultas
    Route::get('/fakultas', 'AdminFakultasController@index');
    Route::get('/fakultas/getdata', 'AdminFakultasController@getdata')->name('fakultas.getdata');
    Route::post('/fakultas/save', 'AdminFakultasController@store');
    Route::get('/fakultas/{id}/delete', 'AdminFakultasController@destroy');
    Route::get('/fakultas/{id}/edit', 'AdminFakultasController@edit');
    Route::put('/fakultas/{id}', 'AdminFakultasController@update');
#end
#crud kategori event
    Route::get('/kategori-event', 'AdminKategoriEventController@index');
    Route::post('/kategori-event/save', 'AdminKategoriEventController@store');
    Route::get('/kategori-event/{id}/delete', 'AdminKategoriEventController@destroy');
    Route::get('/kategori-event/{id}/edit', 'AdminKategoriEventController@edit');
    Route::put('/kategori-event/{id}', 'AdminKategoriEventController@update');
#end
#crud jurusan
    Route::get('/jurusan', 'AdminJurusanController@index');
    Route::post('/jurusan/save', 'AdminJurusanController@store');
    Route::get('/jurusan/{id}/delete', 'AdminJurusanController@destroy');
    Route::get('/jurusan/{id}/edit', 'AdminJurusanController@edit');
    Route::put('/jurusan/{id}', 'AdminJurusanController@update');
#end
#crud jurusan
    Route::get('/programstudi', 'AdminProgramStudiController@index');
    Route::post('/programstudi/save', 'AdminProgramStudiController@store');
    Route::get('/programstudi/{id}/delete', 'AdminProgramStudiController@destroy');
    Route::get('/programstudi/{id}/edit', 'AdminProgramStudiController@edit');
    Route::put('/programstudi/{id}', 'AdminProgramStudiController@update');
#end
#crud managemenn user
    #dosen
    Route::get('/dosen', 'AdminUsersController@indexDosen');
    Route::post('/dosen/save', 'AdminUsersController@storeDosen');
    Route::get('/dosen/{id}/delete', 'AdminUsersController@destroyDosen');
    Route::get('/dosen/{id}/edit', 'AdminUsersController@editDosen');
    Route::put('/dosen/{id}', 'AdminUsersController@updateDosen');
    #instruktur
    Route::get('/instruktur', 'AdminUsersController@indexInstruktur');
    Route::post('/instruktur/save', 'AdminUsersController@storeInstruktur');
    Route::get('/instruktur/{id}/delete', 'AdminUsersController@destroyInstruktur');
    Route::get('/instruktur/{id}/edit', 'AdminUsersController@editInstruktur');
    Route::put('/instruktur/{id}', 'AdminUsersController@updateInstruktur');
    #peserta
    Route::get('/peserta', 'AdminUsersController@indexPeserta');
    Route::post('/peserta/save', 'AdminUsersController@storePeserta');
    Route::get('/peserta/{id}/delete', 'AdminUsersController@destroyPeserta');
    Route::get('/peserta/{id}/edit', 'AdminUsersController@editPeserta');
    Route::put('/peserta/{id}', 'AdminUsersController@updatePeserta');
    #sekum
    Route::get('/sekum', 'AdminUsersController@indexSekum');
    Route::post('/sekum/save', 'AdminUsersController@storeSekum');
    Route::get('/sekum/{id}/delete', 'AdminUsersController@destroySekum');
    Route::get('/sekum/{id}/edit', 'AdminUsersController@editSekum');
    Route::put('/sekum/{id}', 'AdminUsersController@updateSekum');
    #sekumldf
    Route::get('/sekumldf', 'AdminUsersController@indexSekumldf');
    Route::post('/sekumldf/save', 'AdminUsersController@storeSekumldf');
    Route::get('/sekumldf/{id}/delete', 'AdminUsersController@destroySekumldf');
    Route::get('/sekumldf/{id}/edit', 'AdminUsersController@editSekumldf');
    Route::put('/sekumldf/{id}', 'AdminUsersController@updateSekumldf');
    #materi
    Route::get('/materi', 'AdminMateriController@index');
    Route::post('/materi/save', 'AdminMateriController@store');
    Route::get('/materi/{id}/delete', 'AdminMateriController@destroy');
    Route::get('/materi/{id}/edit', 'AdminMateriController@edit');
    Route::put('/materi/{id}', 'AdminMateriController@update');
    #event
    Route::get('/event', 'AdminEventController@index');
    Route::post('/event/save', 'AdminEventController@store');
    Route::get('/event/{id}/delete', 'AdminEventController@destroy');
    Route::get('/event/{id}/edit', 'AdminEventController@edit');
    Route::put('/event/{id}', 'AdminEventController@update');
    #jadwal
    Route::get('/jadwal', 'AdminJadwalController@index');
    Route::post('/jadwal/save', 'AdminJadwalController@store');
    Route::get('/jadwal/{id}/delete', 'AdminJadwalController@destroy');
    Route::get('/jadwal/{id}/edit', 'AdminJadwalController@edit');
    Route::put('/jadwal/{id}', 'AdminJadwalController@update');
    #jadwal dosen
    Route::get('/jadwal-dosen', 'AdminJadwalDosenController@index');
    Route::post('/jadwal-dosen/save', 'AdminJadwalDosenController@store');
    Route::get('/jadwal-dosen/{id}/delete', 'AdminJadwalDosenController@destroy');
    Route::get('/jadwal-dosen/{id}/edit', 'AdminJadwalDosenController@edit');
    Route::put('/jadwal-dosen/{id}', 'AdminJadwalDosenController@update');
    # berita
    Route::get('/berita', 'AdminBeritaController@index');
    Route::post('/berita/save', 'AdminBeritaController@store');
    Route::get('/berita/{id}/delete', 'AdminBeritaController@destroy');
    #Route::get('/berita/{id}/edit', 'AdminBeritaController@edit');
    #Route::put('/berita/{id}', 'AdminBeritaController@update');
#end
});

Route::group(['prefix' => 'home',  'middleware' => 'peserta'], function () {
#crud kartu profile
    Route::get('/profile/peserta', 'PesertaProfileController@index');
    Route::put('/profile/peserta/change/{id}', 'PesertaProfileController@updatefoto');
    Route::get('/profile/peserta/{id}/edit', 'PesertaProfileController@edit');
    Route::put('/profile/peserta/{id}', 'PesertaProfileController@update');
    Route::get('/profile/peserta/{id}/cetak', 'PesertaProfileController@cetak');
#end
#crud materi peserta
    Route::get('/peserta/materi', 'PesertaMateriController@index');
    Route::get('/peserta/materi/{file}/download', 'PesertaMateriController@download');
#end  
#crud jadwal kegiatan
    Route::get('/jadwal-kegiatan/peserta', 'PesertaJadwalController@index');
    Route::get('/jadwal-kegiatan/peserta/{id}/cetak', 'PesertaJadwalController@cetak');
#end
#crud evaluasi ibadah
    Route::get('/evaluasi-ibadah/peserta', 'PesertaEvaluasiIbadahController@index');
    Route::post('/evaluasi-ibadah/peserta/save', 'PesertaEvaluasiIbadahController@store');
    Route::get('/evaluasi-ibadah/peserta/{id}/delete', 'PesertaEvaluasiIbadahController@destroy');
#end  
#crud evaluasi ibadah
    Route::get('/event/peserta', 'PesertaEvaluasiIbadahController@event');
#end  
});

Route::group(['prefix' => 'home',  'middleware' => 'instruktur'], function () {
#crud kartu profile
    Route::get('/profile/instruktur', 'InstrukturProfileController@index');
    Route::put('/profile/instruktur/change/{id}', 'InstrukturProfileController@updatefoto');
    Route::get('/profile/instruktur/{id}/edit', 'InstrukturProfileController@edit');
    Route::put('/profile/instruktur/{id}', 'InstrukturProfileController@update');
    Route::get('/profile/instruktur/{id}/cetak', 'InstrukturProfileController@cetak');
#end
 
#crud jadwal kegiatan
    Route::get('/jadwal-kegiatan/instruktur', 'InstrukturProfileController@jadwal');
    Route::get('/jadwal-kegiatan/instruktur/{id}/cetak', 'InstrukturProfileController@cetakjadwal');
#end

#crud materi instruktur
    Route::get('/instruktur/materi', 'InstrukturMateriController@index');
    Route::get('/instruktur/materi/{file}/download', 'InstrukturMateriController@download');
#end
#crud peserta
    Route::get('/instruktur/peserta', 'InstrukturMateriController@peserta');
    Route::get('/instruktur/peserta/cetak', 'InstrukturMateriController@cetak');
#end

});

Route::group(['prefix' => 'home',  'middleware' => 'dosen'], function () {
#crud kartu profile
#crud profile
    Route::get('/profile/dosen', 'DosenProfileController@index');
    Route::get('/profile/dosen/{id}/edit', 'DosenProfileController@edit');
    Route::put('/profile/dosen/{id}', 'DosenProfileController@update');
    Route::post('/password/dosen/ubahpassword', 'DosenProfileController@ubahpassword');
#end

#materi
    Route::get('/dosen-materi', 'DosenMateriController@index');
    Route::post('/dosen-materi/save', 'DosenMateriController@store');
    Route::get('/dosen-materi/{id}/delete', 'DosenMateriController@destroy');
#evaluasi peserta
    Route::get('/dosen/evaluasi-peserta', 'DosenEvaluasiController@index');
    Route::get('/dosen/evaluasi-peserta/cetak', 'DosenEvaluasiController@cetak');
#evaluasi jadwal
    Route::get('/dosen/jadwal', 'DosenEvaluasiController@jadwal');
    Route::get('/dosen/jadwal/{id}/cetak', 'DosenEvaluasiController@cetakjadwal');

});

Route::group(['prefix' => 'home',  'middleware' => 'sekum'], function () {
#crud kartu profile
#crud profile
    Route::get('/profile/sekum', 'SekumProfileController@index');
    Route::get('/profile/sekum/{id}/edit', 'SekumProfileController@edit');
    Route::put('/profile/sekum/{id}', 'SekumProfileController@update');
    Route::post('/password/sekum/ubahpassword', 'SekumProfileController@ubahpassword');
#end

#users
    Route::get('/sekum/dosen', 'SekumUserController@indexdosen');
    Route::get('/sekum/dosen/cetak', 'SekumUserController@cetakdosen');

    Route::get('/sekum/instruktur', 'SekumUserController@indexinstruktur');
    Route::get('/sekum/instruktur/cetak', 'SekumUserController@cetakinstruktur');

    Route::get('/sekum/peserta', 'SekumUserController@indexpeserta');
    Route::get('/sekum/peserta/cetak', 'SekumUserController@cetakpeserta');

});

Route::group(['prefix' => 'home',  'middleware' => 'sekumldf'], function () {
#crud kartu profile
#crud profile
    Route::get('/profile/sekumldf', 'SekumldfProfileController@index');
    Route::get('/profile/sekumldf/{id}/edit', 'SekumldfProfileController@edit');
    Route::put('/profile/sekumldf/{id}', 'SekumldfProfileController@update');
    Route::post('/password/sekumldf/ubahpassword', 'SekumldfProfileController@ubahpassword');
#end
#sekum
    Route::get('/sekumldf/sekum', 'SekumldfUserController@index');
    Route::post('/sekumldf/sekum/save', 'SekumldfUserController@store');
    #Route::get('/sekumldf/sekum/{id}/delete', 'SekumldfUserController@destroy');
    Route::get('/sekumldf/sekum/{id}/edit', 'SekumldfUserController@edit');
    Route::put('/sekumldf/sekum/{id}', 'SekumldfUserController@update');
    Route::get('/sekumldf/sekum/cetak', 'SekumldfUserController@cetak');
});