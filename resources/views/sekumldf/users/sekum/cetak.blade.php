<!DOCTYPE html>
<html>
<head>
  <title>cetak data sekum</title>
  <style type="text/css">
    table, td, th {    
    border-collapse: collapse;
    border: 2px solid #000000;
    width: 100%;    
    text-align: left;
  }

  img {
    border-radius: 50%;
  }
  </style>
</head>
<body>
  <center>
    <h2>Data sekum</h2>
    <hr>
    <br> <br> 
  </center>
  <br>
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nip</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
            <th>Email</th>
        </tr>
    </thead>
    <body>
      @foreach ($sekum as $key => $value)
        <tr>
          <td>{{ ++$key }}</td>
          <td>
            @if (empty($value->nip))
             -
            @else
              {{ $value->nip }}
            @endif 
          </td>
          <td>{{ $value->nama }}</td>
          <td>
            @if (empty($value->jenis_kelamin))
             -
            @else
              {{ $value->jenis_kelamin }}
            @endif 
          </td>
          <td>
            @if (empty($value->alamat))
             -
            @else
              {{ $value->alamat }}
            @endif
          </td>
          <td>{{ $value->email }}</td>
        </tr>
      @endforeach 
    </body>
  </table>
  
</body>
</html>
