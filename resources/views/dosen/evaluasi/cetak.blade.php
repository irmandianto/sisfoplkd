<!DOCTYPE html>
<html>
<head>
  <title>cetak jadwal kegiatan</title>
  <style type="text/css">
    table, td, th {    
    border-collapse: collapse;
    border: 2px solid #000000;
    width: 100%;    
    text-align: left;
  }

  img {
    border-radius: 50%;
  }
  </style>
</head>
<body>
  <center>
    <h2>Evaluasi Peserta</h2>
    <hr>
    <br> <br> 
  </center>
  <br>
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nim</th>
            <th>Nama</th>
            <th>Minggu Ke</th>
            <th>Sholat Berjamaah</th>
            <th>Sholat Dhuha</th>
            <th>Tilawah</th>
        </tr>
    </thead>
    <body>
      @foreach ($evaluasis as $key => $evaluasi)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $evaluasi->peserta->nim }}</td>
          <td>{{ $evaluasi->nama }}</td>
          <td>{{ $evaluasi->minggu_ke }}</td>
          <td>
              @if ($evaluasi->sholat_berjamaah == 1)
                Berjamaah
              @else
                <span class="badge badge-danger">
                  Tak Berjamaah
                </span>  
              @endif
          </td>
          <td>
            @if($evaluasi->sholat_dhuha == 1)
              Mengerjakan sholat Dhuha
            @else
              <span class="badge badge-danger">
                Tak Mengerjakan sholat Dhuha  
              </span>
              
            @endif  
          </td>
          <td>
            @if($evaluasi->tilawah == 1)
              Mengerjakan Tilawah
            @else
              <span class="badge badge-danger">
                Tak Mengerjakan Tilawah  
              </span>
            @endif
          </td>
        </tr>
      @endforeach 
    </body>
  </table>
  
</body>
</html>
