<li class="active">
	<a href="/home">
		<i class="menu-icon fa fa-tachometer"></i>
		<span class="menu-text"> Dashboard </span>
	</a>

	<b class="arrow"></b>
</li>


<li class="">
	<a href="/home/dosen-materi">
		<i class="menu-icon fa fa-book"></i>
		<span class="menu-text"> Bahan Materi </span>
	</a>

</li>

<li class="">
	<a href="/home/dosen/evaluasi-peserta">
		<i class="menu-icon fa fa-list-alt"></i>
		<span class="menu-text"> Evaluasi Peserta </span>
	</a>
	<b class="arrow"></b>
</li>

<li class="">
	<a href="/home/dosen/jadwal">
		<i class="menu-icon fa fa-credit-card"></i>
		<span class="menu-text"> Jadwal Dosen </span>
	</a>
	<b class="arrow"></b>
</li>

