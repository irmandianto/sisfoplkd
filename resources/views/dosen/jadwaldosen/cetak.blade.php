<!DOCTYPE html>
<html>
<head>
  <title>cetak jadwal kegiatan</title>
  <style type="text/css">
    table, td, th {    
    border-collapse: collapse;
    border: 2px solid #000000;
    width: 100%;    
    text-align: left;
  }

  img {
    border-radius: 50%;
  }
  </style>
</head>
<body>
  <center>
    <h2>Jadwal Dosen</h2>
    <hr>
    <br> <br> 
  </center>
  <br>
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Hari</th>
            <th>Jam</th>
            <th>Isi</th>
        </tr>
    </thead>
    <body>
      @foreach ($jadwals as $key => $value)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $value->hari }}</td>
          <td>{{ $value->waktu }} WIB</td>
          <td>{!! str_limit(nl2br(e($value->isi)), 8000) !!}</td>
        </tr>
      @endforeach 
    </body>
  </table>
  
</body>
</html>
