<li class="active">
	<a href="/home">
		<i class="menu-icon fa fa-tachometer"></i>
		<span class="menu-text"> Dashboard </span>
	</a>

	<b class="arrow"></b>
</li>

<li class="">
	<a href="/home/peserta/materi">
		<i class="menu-icon fa fa-folder-open"></i>
		<span class="menu-text"> Bahan Materi </span>
	</a>

</li>

<li class="">
	<a href="/home/event/peserta">
		<i class="menu-icon fa fa-bullhorn"></i>
		<span class="menu-text"> Event </span>
	</a>

</li>

<li class="">
	<a href="/home/jadwal-kegiatan/peserta">
		<i class="menu-icon fa fa-pencil-square-o"></i>
		<span class="menu-text"> Jadwal Kegiatan </span>
	</a>

</li>

<li class="">
	<a href="/home/evaluasi-ibadah/peserta">
		<i class="menu-icon fa fa-check-square-o"></i>
		<span class="menu-text"> Evaluasi Ibadah </span>
	</a>

	<b class="arrow"></b>
</li>

