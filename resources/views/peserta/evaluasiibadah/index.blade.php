@extends('layouts.peserta')
@section('title', 'evaluasi ibadah')
@section('nav', 'evaluasi ibadah')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- ace styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
@include('flash-group')
@include('flash-message')
<button class="btn btn-success btn-md pull-right" data-toggle="modal" data-target="#myModal">
  Add
</button>
<br>
<br>
<br>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Evaluasi Ibadah</h4>
      </div>
      <div class="modal-body">
        <form class="contact-form php-mail-form" role="form" action="/home/evaluasi-ibadah/peserta/save" method="POST">
        @csrf

          <div class="form-group">
            <label>Minggu Ke</label>
            <select name="minggu_ke" class="form-control" required>
              <option value="null">- Pilih Minggu -</option>
              <option value="satu">- Minggu ke - 1</option>
              <option value="dua">- Minggu ke - 2</option>
              <option value="tiga">- Minggu ke - 3</option>
              <option value="empat">- Minggu ke - 4</option>
            </select>  
          </div>
          
          <div class="checkbox">
            <label>
              <input name="sholat_berjamaah" type="checkbox" class="ace" value="1" />
              <span class="lbl"> Sholat Berjamaah</span>
            </label>
          </div>

          <div class="checkbox">
            <label>
              <input name="sholat_dhuha" type="checkbox" class="ace" value="1" />
              <span class="lbl"> Sholat Dhuha</span>
            </label>
          </div>

          <div class="checkbox">
            <label>
              <input name="tilawah" type="checkbox" class="ace" value="1" />
              <span class="lbl"> Tilawah</span>
            </label>
          </div>

          <div class="form-send">
            <button type="submit" class="btn btn-large btn-primary pull-right">Save</button>
          </div>

        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Minggu Ke</th>
            <th>Sholat Berjamaah</th>
            <th>Sholat Dhuha</th>
            <th>Tilawah</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <body>
      @foreach ($evaluasis as $key => $evaluasi)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $evaluasi->nama }}</td>
          <td>{{ $evaluasi->minggu_ke }}</td>
          <td>
              @if ($evaluasi->sholat_berjamaah == 1)
                Berjamaah
              @else
                <span class="badge badge-danger">
                  Tak Berjamaah
                </span>  
              @endif
          </td>
          <td>
            @if($evaluasi->sholat_dhuha == 1)
              Mengerjakan sholat Dhuha
            @else
              <span class="badge badge-danger">
                Tak Mengerjakan sholat Dhuha  
              </span>
              
            @endif  
          </td>
          <td>
            @if($evaluasi->tilawah == 1)
              Mengerjakan Tilawah
            @else
              <span class="badge badge-danger">
                Tak Mengerjakan Tilawah  
              </span>
            @endif
          </td>
          <td>
            <a href="/home/evaluasi-ibadah/peserta/{{ $evaluasi->id }}/delete" class="btn btn-primary " onclick="return konfirmasi();">
              Delete
            </a>
          </td>
        </tr>
      @endforeach 
    </body>
  </table>
@endsection
@section('js')
<script type="text/javascript" language="JavaScript">
  function konfirmasi()
  {
  tanya = confirm("Anda Yakin Akan Menghapus Data Ini ?");
  if (tanya == true) return true;
  else return false;
  }
</script>
<!-- <script type="text/javascript">
$(document).ready(function() {
     $('#dataTables-example').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('fakultas.getdata') }}",
        "columns":[
            { "data": "nama_fakultas" }
        ]
     });
});
</script> -->

    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="/ace/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
<script>
  $(document).ready(function() {
      $('#dataTables-example').DataTable({
          responsive: true
      });
  });
</script>
<script src="/assets/js/jquery-2.1.4.min.js"></script>
<script src="/assets/js/jquery-ui.min.js"></script>

<script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
@endsection