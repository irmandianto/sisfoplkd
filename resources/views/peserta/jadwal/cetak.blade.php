<!DOCTYPE html>
<html>
<head>
  <title>cetak jadwal kegiatan</title>
  <style type="text/css">
    table, td, th {    
    border-collapse: collapse;
    border: 2px solid #000000;
    width: 100%;    
    text-align: left;
  }

  img {
    border-radius: 50%;
  }
  </style>
</head>
<body>
  <center>
    <h2>Jadwal Kegiatan</h2>
    <hr>
    <br> <br> 
  </center>
  <br>
  <h3>NIM : {{ auth()->user()->nim }}</h3>
  <h3>NAMA : {{ auth()->user()->nama }}</h3>
    <table align="center">
      <thead>
        <tr>
          <td>
            Hari
          </td>
          <td>
            Waktu
          </td>
          <td>
            Deskripsi
          </td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{ $jadwal->hari }}</td>
          <td>{{ $jadwal->waktu }} s/d selesai</td>
          <td>{!! str_limit(nl2br(e($jadwal->isi)), 9000) !!}</td>
        </tr>
      </tbody>
    </table>
  
</body>
</html>
