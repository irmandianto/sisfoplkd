<!DOCTYPE html>
<html>
<head>
  <title>cetak kartu peserta</title>
  <style type="text/css">
    table, td, th {    
    border: none;
    width: 50%;    
    text-align: left;
    align: center;
  }

  img {
    border-radius: 50%;
  }
  </style>
</head>
<body>
  <center>
    <h2>KARTU PESERTA</h2>
    <hr>
    <br> <br> 
  </center>
  <center>
    <img alt="Foto profile" src="{{ public_path('foto/' . $peserta->foto) }}" style="width:300px" />
  </center>
  <br>
  <p>
    <center>
        <table align="center">
          <tr>
            <td>
              NIM
            </td>
            <td>
              {{ $peserta->nim }}  
            </td>
          </tr>
          <tr>
            <td>
              NAMA
            </td>
            <td>
              {{ $peserta->nama }}
            </td>
          </tr>
        </table>
    </center>
  </p>
</body>
</html>
