<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Login</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

        <!-- styles -->
        <link rel="stylesheet" href="/assets/css/ace.min.css" />

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="login-layout">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf green"></i>
                                    <span class="red">SISTEM</span>
                                    <span class="white" id="id-text2">INFORMASI</span>
                                </h1>
                                <h4 class="blue" id="id-company-text">PLKD</h4>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            @include('flash-message')

                                            <div class="space-6"></div>

                                            <form action="{{ route('login') }}" method="post">
                                                @csrf
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="email" class="form-control" placeholder="email" name="email" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                            @if ($errors->has('email'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Password" name="password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                            @if ($errors->has('password'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('password') }}</strong>
                                                                </span>
                                                            @endif
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-rightx">
                                                            <select class="form-control" name="role">
                                                                <option># Pilih Role #</option>
                                                                <option value="0"># Administator</option>
                                                                <option value="1"># Instruktur</option>
                                                                <option value="2"># Dosen</option>
                                                                <option value="3"># Peserta</option>
                                                                <option value="4"># Sekum</option>
                                                                <option value="5"># Sekum ldf</option>
                                                            </select>
                                                            @if ($errors->has('role'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('role') }}</strong>
                                                                </span>
                                                            @endif
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <div class="clearfix">
                                                        <label class="inline">
                                                            <input type="checkbox" class="ace" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                                                            <span class="lbl"> Remember Me</span>
                                                        </label>

                                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Login</span>
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>


                                            <div class="space-6"></div>

                                        </div><!-- /.widget-main -->

                                        <div class="toolbar clearfix">
                                            <!-- <div>
                                                <a href="#" data-target="#forgot-box" class="forgot-password-link">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    I forgot my password
                                                </a>
                                            </div> -->

                                            <div>
                                                <a href="#" data-target="#signup-box" class="user-signup-link">
                                                    I want to register
                                                    <i class="ace-icon fa fa-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div><!-- /.login-box -->

                                <div id="forgot-box" class="forgot-box widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header red lighter bigger">
                                                <i class="ace-icon fa fa-key"></i>
                                                Retrieve Password
                                            </h4>

                                            <div class="space-6"></div>
                                            <p>
                                                Enter your email and to receive instructions
                                            </p>

                                            <form>
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="email" class="form-control" placeholder="Email" />
                                                            <i class="ace-icon fa fa-envelope"></i>
                                                        </span>
                                                    </label>

                                                    <div class="clearfix">
                                                        <button type="button" class="width-35 pull-right btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-lightbulb-o"></i>
                                                            <span class="bigger-110">Send Me!</span>
                                                        </button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /.widget-main -->

                                        <div class="toolbar center">
                                            <a href="#" data-target="#login-box" class="back-to-login-link">
                                                Back to login
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div><!-- /.forgot-box -->

                                <div id="signup-box" class="signup-box widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header green lighter bigger">
                                                <i class="ace-icon fa fa-users blue"></i>
                                                Registrasi Peserta / Instruktur
                                            </h4>

                                            <div class="space-6"></div>

                                            <form action="/registrasiPeserta" method="post">
                                                @csrf
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="nim" name="nim" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                        @if ($errors->has('nim'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('nim') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="nama" name="nama" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                        @if ($errors->has('nama'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('nama') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-rightx">
                                                            <select class="form-control" name="jenis_kelamin">
                                                                <option>- pilih jenis kelamin -</option>
                                                                <option value="laki-laki">laki-laki</option>
                                                                <option value="perempuan">perempuan</option>
                                                            </select>
                                                        </span>
                                                        @if ($errors->has('jenis_kelamin'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('jenis_kelamin') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="email" class="form-control" placeholder="Email" name="email" />
                                                            <i class="ace-icon fa fa-envelope"></i>
                                                        </span>
                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>
                                                                    {{ $errors->first('email') }}
                                                                </strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Password" name="password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-rightx">
                                                            <select class="form-control" name="role" required>
                                                                <option value="null">- pilih role -</option>
                                                                <option value="3">Peserta</option>
                                                                <option value="1">Instruktur</option>
                                                            </select>
                                                        </span>
                                                    </label>

                                                    <div class="space-24"></div>

                                                    <div class="clearfix">
                                                        <button type="reset" class="width-30 pull-left btn btn-sm">
                                                            <i class="ace-icon fa fa-refresh"></i>
                                                            <span class="bigger-110">Reset</span>
                                                        </button>

                                                        <button type="submit" class="width-65 pull-right btn btn-sm btn-success">
                                                            <span class="bigger-110">Register</span>

                                                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                        </button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                        <div class="toolbar center">
                                            <a href="#" data-target="#login-box" class="back-to-login-link">
                                                <i class="ace-icon fa fa-arrow-left"></i>
                                                Back to login
                                            </a>
                                            <a href="#" data-target="#signup-box-dosen" class="back-to-login-link">
                                                Register dosen
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div>
                                <div id="signup-box-dosen" class="signup-box widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header green lighter bigger">
                                                <i class="ace-icon fa fa-users blue"></i>
                                                Registrasi Dosen
                                            </h4>

                                            <div class="space-6"></div>

                                            <form action="/registrasiDosen" method="post">
                                                @csrf
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="nip" name="nip" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                        @if ($errors->has('nip'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('nip') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="nama" name="nama" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                        @if ($errors->has('nama'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('nama') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-rightx">
                                                            <select class="form-control" name="jenis_kelamin">
                                                                <option>- pilih jenis kelamin -</option>
                                                                <option value="laki-laki">laki-laki</option>
                                                                <option value="perempuan">perempuan</option>
                                                            </select>
                                                        </span>
                                                        @if ($errors->has('jenis_kelamin'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('jenis_kelamin') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="email" class="form-control" placeholder="Email" name="email" />
                                                            <i class="ace-icon fa fa-envelope"></i>
                                                        </span>
                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>
                                                                    {{ $errors->first('email') }}
                                                                </strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Password" name="password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <div class="space-24"></div>

                                                    <div class="clearfix">
                                                        <button type="reset" class="width-30 pull-left btn btn-sm">
                                                            <i class="ace-icon fa fa-refresh"></i>
                                                            <span class="bigger-110">Reset</span>
                                                        </button>

                                                        <button type="submit" class="width-65 pull-right btn btn-sm btn-success">
                                                            <span class="bigger-110">Register</span>

                                                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                        </button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                        <div class="toolbar center">
                                            <a href="#" data-target="#login-box" class="back-to-login-link">
                                                <i class="ace-icon fa fa-arrow-left"></i>
                                                Back to login
                                            </a>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div>
                                <!-- /.signup-box -->

                            </div><!-- /.position-relative -->

                            <div class="navbar-fixed-top align-right">
                                <br />
                                &nbsp;
                                <a id="btn-login-dark" href="#">Dark</a>
                                &nbsp;
                                <span class="blue">/</span>
                                &nbsp;
                                <a id="btn-login-blur" href="#">Blur</a>
                                &nbsp;
                                <span class="blue">/</span>
                                &nbsp;
                                <a id="btn-login-light" href="#">Light</a>
                                &nbsp; &nbsp; &nbsp;
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="/assets/js/jquery-2.1.4.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
<script src="/ace/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function($) {
             $(document).on('click', '.toolbar a[data-target]', function(e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible');//hide others
                $(target).addClass('visible');//show target
             });
            });
            
            
            
            //you don't need this, just used for changing background
            jQuery(function($) {
             $('#btn-login-dark').on('click', function(e) {
                $('body').attr('class', 'login-layout');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'blue');
                
                e.preventDefault();
             });
             $('#btn-login-light').on('click', function(e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');
                
                e.preventDefault();
             });
             $('#btn-login-blur').on('click', function(e) {
                $('body').attr('class', 'login-layout blur-login');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'light-blue');
                
                e.preventDefault();
             });
             
            });
        </script>
    </body>
</html>