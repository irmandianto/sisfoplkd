@extends('index')
@section('title', 'event')
@section('nav', 'event')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
<div class="row">
  <div class="col-md-12"> 
    <div>
      <ul class="ace-thumbnails clearfix">
      @foreach ($events as $key => $value)  
        <li>
          <a href="{{ asset('event/' . $value->foto)}}" target="_blank" title="Photo Title" data-rel="colorbox">
            <img width="250" height="250" alt="150x150" src="{{ asset('event/' . $value->foto)}}" />
          </a>

          <div class="tags">
            <span class="label-holder">
              <span class="label label-warning arrowed-in">{{ $value->kategori->nama_kategori }}</span>
            </span>
          </div>

          <div class="tools">
            <a href="#">
              <i class="ace-icon fa fa-check-square-o"></i>
            </a>
          </div>
        </li>
      @endforeach
      </ul>
      {{ $events->links() }}
    </div>
  </div><!-- PAGE CONTENT ENDS -->
</div>
@endsection
