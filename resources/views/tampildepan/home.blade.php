@extends('index')
@section('title', 'home')
@section('nav', 'home')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
<div class="row">
  <div class="col-md-12"> 
    <div>
      <ul class="ace-thumbnails clearfix">
      @foreach ($events as $key => $value)  
        <li>
          <a href="{{ asset('event/' . $value->foto)}}" target="_blank" title="Photo Title" data-rel="colorbox">
            <img width="250" height="250" alt="150x150" src="{{ asset('event/' . $value->foto)}}" />
          </a>

          <div class="tags">
            <span class="label-holder">
              <span class="label label-warning arrowed-in">{{ $value->kategori->nama_kategori }}</span>
            </span>
          </div>

          <div class="tools">
            <a href="#">
              <i class="ace-icon fa fa-check-square-o"></i>
            </a>
          </div>
        </li>
      @endforeach
      </ul>
      {{ $events->links() }}
    </div>
  </div><!-- PAGE CONTENT ENDS -->
</div>
<hr>
<div class="row">
  <div class="col-md-12"> 
    <div>
      <ul class="ace-thumbnails clearfix">
        @foreach ($beritas as $key => $berita)  
        <li>

          @if ($berita->jenis_file == 1)
            <a href="{{ asset('berita/' . $berita->foto)}}" target="_blank" title="Photo Title" data-rel="colorbox">
              <video width="250" height="250" controls>
                <source src="{{ asset('berita/' . $berita->foto) }}" type="video/mp4">
              </video>
            </a>
          @else
          <a href="{{ asset('berita/' . $berita->foto)}}" target="_blank" title="Photo Title" data-rel="colorbox">
            <img src="{{ asset('berita/' . $berita->foto)}}" width="250" height="250">
          </a>
            
          @endif
          
          <div class="tags">
            <span class="label-holder">
              <span class="label label-warning arrowed-in">
                {{ $berita->judul }}</span>
            </span>
          </div>

          <div class="tools">
            <a href="#">
              <i class="ace-icon fa fa-check-square-o"></i>
            </a>
          </div>
        </li>
        @endforeach
      </ul>
      {{ $beritas->links() }}
    </div>
  </div><!-- PAGE CONTENT ENDS -->
</div>
@endsection
@section('js')
<script type="text/javascript" language="JavaScript">
  function konfirmasi()
  {
  tanya = confirm("Anda Yakin Akan Menghapus Data Ini ?");
  if (tanya == true) return true;
  else return false;
  }
</script>
<!-- <script type="text/javascript">
$(document).ready(function() {
     $('#dataTables-example').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('fakultas.getdata') }}",
        "columns":[
            { "data": "nama_fakultas" }
        ]
     });
});
</script> -->

    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="/ace/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
<script>
  $(document).ready(function() {
      $('#dataTables-example').DataTable({
          responsive: true
      });
  });
</script>
<script src="/assets/js/jquery-2.1.4.min.js"></script>
<script src="/assets/js/jquery-ui.min.js"></script>

<script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
@endsection
