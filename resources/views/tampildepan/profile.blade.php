@extends('index')
@section('title', 'profile')
@section('nav', 'profile')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
<div id="timeline-2" class="hidex">
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
      <div class="timeline-container timeline-style2">
        <span class="timeline-label">
          <b>Visi</b>
        </span>

        <div class="timeline-items">
          <div class="timeline-item clearfix">
            <div class="timeline-info">
              <span class="timeline-date">A</span>

              <i class="timeline-indicator btn btn-info no-hover"></i>
            </div>

            <div class="widget-box transparent">
              <div class="widget-body">
                <div class="widget-main no-padding">
                  <span class="bigger-110">
                    "Menjadi wadah pengembangan wawasan keislaman mahasiswa UNP yang berbasis pembinaan dan kompetensi dalam rangka mewujudkan insan akademis yang religius, berakhlakul karimah dan ilmiah menuju masyarakat kampus yang islami."
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.timeline-items -->
      </div><!-- /.timeline-container -->

      <div class="timeline-container timeline-style2">
        <span class="timeline-label">
          <b>Misi</b>
        </span>

        <div class="timeline-items">
          <div class="timeline-item clearfix">
            <div class="timeline-info">
              <span class="timeline-date">1</span>

              <i class="timeline-indicator btn btn-success no-hover"></i>
            </div>

            <div class="widget-box transparent">
              <div class="widget-body">
                <div class="widget-main no-padding">
                  <div class="clearfix">
                    <div class="pull-left">
                      Membentuk wawasan keislaman mahasiswa UNP dengan metode pembinaan yang tepat, berkelanjutan dan berkesinambungan.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="timeline-item clearfix">
            <div class="timeline-info">
              <span class="timeline-date">2</span>

              <i class="timeline-indicator btn btn-success no-hover"></i>
            </div>

            <div class="widget-box transparent">
              <div class="widget-body">
                <div class="widget-main no-padding">
                  Menebarkan nilai-nilai keislaman yang membentuk pribadi muslim melaui syiar dan pelayanan kampus yang mengakar dan menyeluruh kepada seluruh elemen kampus.
                </div>
              </div>
            </div>
          </div>

          <div class="timeline-item clearfix">
            <div class="timeline-info">
              <span class="timeline-date">3</span>

              <i class="timeline-indicator btn btn-success no-hover"></i>
            </div>

            <div class="widget-box transparent">
              <div class="widget-body">
                <div class="widget-main no-padding"> 
                  Mengembangkan budaya profesionalisme dan kemandirian kerja dalam melaksanakan agenda-agenda Qatulistiwa islam UKK UNP.
                </div>
              </div>
            </div>
          </div>

          <div class="timeline-item clearfix">
            <div class="timeline-info">
              <span class="timeline-date">4</span>

              <i class="timeline-indicator btn btn-success no-hover"></i>
            </div>

            <div class="widget-box transparent">
              <div class="widget-body">
                <div class="widget-main no-padding">
                  Menyokong terciptanya masyarakat kampus yang islami.
                </div>
              </div>
            </div>
          </div>

          <div class="timeline-item clearfix">
            <div class="timeline-info">
              <span class="timeline-date">5</span>

              <i class="timeline-indicator btn btn-success no-hover"></i>
            </div>

            <div class="widget-box transparent">
              <div class="widget-body">
                <div class="widget-main no-padding">
                  Menjadikan Qatulistiwa islam UKK UNP sebagai salah satu akselator pembinaan wawasankeislaman mahasiswa di Sumater Barat.
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.timeline-items -->
      </div><!-- /.timeline-container -->
<!-- /.timeline-container -->
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" language="JavaScript">
  function konfirmasi()
  {
  tanya = confirm("Anda Yakin Akan Menghapus Data Ini ?");
  if (tanya == true) return true;
  else return false;
  }
</script>
<!-- <script type="text/javascript">
$(document).ready(function() {
     $('#dataTables-example').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('fakultas.getdata') }}",
        "columns":[
            { "data": "nama_fakultas" }
        ]
     });
});
</script> -->

    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="/ace/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
<script>
  $(document).ready(function() {
      $('#dataTables-example').DataTable({
          responsive: true
      });
  });
</script>
<script src="/assets/js/jquery-2.1.4.min.js"></script>
<script src="/assets/js/jquery-ui.min.js"></script>

<script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
@endsection
