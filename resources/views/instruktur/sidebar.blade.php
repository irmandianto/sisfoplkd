<li class="active">
	<a href="/home">
		<i class="menu-icon fa fa-tachometer"></i>
		<span class="menu-text"> Dashboard </span>
	</a>

	<b class="arrow"></b>
</li>

<li class="">
	<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-users"></i>
		<span class="menu-text">
			Management User
		</span>

		<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">

		<li class="">
			<a href="/home/instruktur/peserta">
				<i class="menu-icon fa fa-caret-right"></i>
				Peserta
			</a>

			<b class="arrow"></b>
		</li>

	</ul>
</li>

<li class="">
	<a href="/home/jadwal-kegiatan/instruktur">
		<i class="menu-icon fa fa-book"></i>
		<span class="menu-text"> Jadwal Kegiatan </span>
	</a>

</li>

<li class="">
	<a href="/home/instruktur/materi">
		<i class="menu-icon fa fa-list-alt"></i>
		<span class="menu-text"> Bahan Materi </span>
	</a>

	<b class="arrow"></b>
</li>


