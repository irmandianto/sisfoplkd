<li class="active">
	<a href="/home">
		<i class="menu-icon fa fa-tachometer"></i>
		<span class="menu-text"> Dashboard </span>
	</a>

	<b class="arrow"></b>
</li>

<li class="">
	<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-users"></i>
		<span class="menu-text">
			Management User
		</span>

		<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">

		<li class="">
			<a href="/home/dosen">
				<i class="menu-icon fa fa-caret-right"></i>
				Dosen
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/instruktur">
				<i class="menu-icon fa fa-caret-right"></i>
				Instruktur
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/peserta">
				<i class="menu-icon fa fa-caret-right"></i>
				Peserta
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/sekum">
				<i class="menu-icon fa fa-caret-right"></i>
				Sekum
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/sekumldf">
				<i class="menu-icon fa fa-caret-right"></i>
				Sekum ldf
			</a>

			<b class="arrow"></b>
		</li>

	</ul>
</li>

<li class="">
	<a href="/home/materi">
		<i class="menu-icon fa fa-book"></i>
		<span class="menu-text"> Bahan Materi </span>
	</a>

</li>

<li class="">
	<a href="/home/event">
		<i class="menu-icon fa fa-list-alt"></i>
		<span class="menu-text"> Event </span>
	</a>

	<b class="arrow"></b>
</li>

<li class="">
	<a href="/home/berita">
		<i class="menu-icon fa fa-video-camera"></i>
		<span class="menu-text"> Berita </span>
	</a>

	<b class="arrow"></b>
</li>

<li class="">
	<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-tag"></i>
		<span class="menu-text"> More Menus </span>

		<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">
		<li class="">
			<a href="/home/fakultas">
				<i class="menu-icon fa fa-caret-right"></i>
				Fakultas
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/kategori-event">
				<i class="menu-icon fa fa-caret-right"></i>
				Kategori Event
			</a>

			<b class="arrow"></b>
		</li>

		<li class="#">
			<a href="/home/jurusan">
				<i class="menu-icon fa fa-caret-right"></i>
				Jurusan
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/programstudi">
				<i class="menu-icon fa fa-caret-right"></i>
				Program Studi
			</a>

			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="/home/jadwal">
				<i class="menu-icon fa fa-caret-right"></i>
				Jadwal
			</a>

			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="/home/jadwal-dosen">
				<i class="menu-icon fa fa-caret-right"></i>
				Jadwal Dosen
			</a>

			<b class="arrow"></b>
		</li>
	</ul>
</li>
