@extends('layouts.admin')
@section('title', 'Jurusan')
@section('nav', 'Jurusan')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- ace styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
@include('flash-message')
<button class="btn btn-success btn-md pull-right" data-toggle="modal" data-target="#myModal">
  Add
</button>
<br>
<br>
<br>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Jurusan</h4>
      </div>
      <div class="modal-body">
        <form class="contact-form php-mail-form" role="form" action="/home/jurusan/save" method="POST">
        @csrf
          <div class="form-group">
            <input type="text" name="nama_jurusan" class="form-control" placeholder="nama jurusan" required>
          </div>

          <div class="form-send">
            <button type="submit" class="btn btn-large btn-primary pull-right">Save</button>
          </div>

        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Jurusan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <body>
      @foreach ($jurusan as $key => $value)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $value->nama_jurusan }}</td>
          <td>
            <a href="/home/jurusan/{{ $value->id }}/edit" class="btn btn-primary ">
              <i class="ace-icon fa fa-pencil-square-o "></i>
              Edit
            </a>
            <a href="/home/jurusan/{{ $value->id }}/delete" class="btn btn-danger" onclick="return konfirmasi()">
              <i class="ace-icon fa fa-trash-o"></i>
              Delete
            </a>
          </td>
        </tr>
      @endforeach 
    </body>
  </table>
@endsection
@section('js')
<script type="text/javascript" language="JavaScript">
  function konfirmasi()
  {
  tanya = confirm("Anda Yakin Akan Menghapus Data Ini ?");
  if (tanya == true) return true;
  else return false;
  }
</script>
<!-- <script type="text/javascript">
$(document).ready(function() {
     $('#dataTables-example').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('fakultas.getdata') }}",
        "columns":[
            { "data": "nama_fakultas" }
        ]
     });
});
</script> -->

    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="/ace/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
<script>
  $(document).ready(function() {
      $('#dataTables-example').DataTable({
          responsive: true
      });
  });
</script>
<script src="/assets/js/jquery-2.1.4.min.js"></script>
<script src="/assets/js/jquery-ui.min.js"></script>

<script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
@endsection