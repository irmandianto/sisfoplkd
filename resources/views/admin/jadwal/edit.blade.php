@extends('layouts.admin')
@section('title', 'edit jadwal')
@section('nav', 'edit jadwal')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
@include('flash-message')
<div class="col-xs-12 col-sm-12">
  <div class="col-xs-12 col-sm-12 pricing-box">
    <div class="widget-box widget-color-blue">
      <div class="widget-header">
        <h5 class="widget-title bigger lighter">Jadwal</h5>
      </div>

      <div class="widget-body">
        <div class="widget-main">
        <form class="contact-form php-mail-form" role="form" action="/home/jadwal/{{ $edit->id }}" method="POST">
        @csrf
        {{method_field('put')}}
          <div class="form-group">
            <label for="comment">Hari</label>
            <input type="text" name="hari" class="form-control" placeholder="hari" value="{{ $edit->hari }}" required>
          </div>
          <div class="form-group">
            <label for="comment">Waktu</label>
            <input type="time" name="waktu" class="form-control" value="{{ $edit->waktu }}" required>
          </div>
          <div class="form-group">
            <label for="comment">Isi</label>
            <textarea class="form-control" rows="5" name="isi" placeholder="isi" required>
              {{ $edit->isi }}
            </textarea>
          </div>

          <div class="form-send">
            <button type="submit" class="btn btn-large btn-primary pull-right">Update</button>
          </div>

        </form>    

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')

<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
<script type="text/javascript">
  if('ontouchstart' in document.documentElement) document.write("<script src='/ace/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<!-- <script>
  $(document).ready(function() {
      $('#dataTables-example').DataTable({
          responsive: true
      });
  });
</script> -->
<script src="/assets/js/jquery-ui.min.js"></script>

<script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
@endsection