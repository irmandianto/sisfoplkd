@extends('layouts.admin')
@section('title', 'profile')
@section('nav', 'profile')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- ace styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
@include('flash-group')
@include('flash-message')
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			<div class="row">
			<div class="col-xs-12">
				<div class="hr dotted"></div>
					<div id="user-profile-1" class="user-profile row">
						<div class="col-xs-12 col-sm-3 center">
							<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
								<div class="inline position-relative">
									<a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
										<i class="ace-icon fa fa-circle light-green"></i>
										&nbsp;
										<span class="white">{{ Auth()->user()->nama }}</span>
									</a>
								</div>
							</div>
							<div class="space-6"></div>

						<div class="profile-contact-info">
						<div class="profile-contact-links align-left">
						<a href="#" class="btn btn-link" data-toggle="modal" data-target="#myModal">
						<i class="ace-icon fa fa-lock bigger-120 green"></i>
						Ubah Password
						</a>

						</div>

						<div class="space-6"></div>

						</div>
						</div>

					<div class="col-xs-12 col-sm-9">

					<div class="space-12"></div>

					<div class="profile-user-info profile-user-info-striped">
					<div class="profile-info-row">
					<div class="profile-info-name"> Nama </div>

					<div class="profile-info-value">
					<span class="editable" id="username">{{ $profile->nama }}</span>
					</div>
					</div>

					<div class="profile-info-row">
					<div class="profile-info-name"> Jenis kelamin </div>

					<div class="profile-info-value">
					<i class="fa fa-map-marker light-orange bigger-110"></i>
					<span class="editable" id="country">{{ $profile->jenis_kelamin }}</span>
					</div>
					</div>

					<div class="profile-info-row">
					<div class="profile-info-name"> Alamat </div>

					<div class="profile-info-value">
					<span class="editable" id="age">
						@if ($profile->alamat == null)
							-
						@else
							{{ $profile->alamat }}
						@endif 
					</span>
					</div>
					</div>

					<div class="profile-info-row">
					<div class="profile-info-name"> Email </div>

					<div class="profile-info-value">
					<span class="editable" id="signup">{{ $profile->email }}</span>
					</div>
					</div>

					</div>

					<div class="space-20"></div>
					<div class="profile-contact-info">
					<div class="profile-contact-links align-left">
						<a href="/home/profile/admin/{{ $profile->id }}/edit" class="btn btn-link">
							Edit
						</a>
					</div>

					<div class="space-6"></div>

					</div>
					<div class="hr hr2 hr-double"></div>

					<div class="space-6"></div>

					</div>
					</div>
				</div><!-- /.well -->
			</div><!-- /.span -->
		</div><!-- /.user-profile -->
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
	      </div>
	      <div class="modal-body">
	        <form class="contact-form php-mail-form" role="form" action="/home/password/admin/ubahpassword" method="POST">
	        @csrf
	          <div class="form-group">
	            <label for="comment">New password</label>
	            <input type="password" name="password" placeholder="new password.." class="form-control" required>
	          </div>
	          

	          <div class="form-send">
	            <button type="submit" class="btn btn-large btn-primary pull-right">Save</button>
	          </div>

	        </form>
	      </div>
	      <div class="modal-footer">
	      </div>
	    </div>
	  </div>
	</div>
</div><!-- /.col -->

@endsection
@section('js')
<script type="text/javascript" language="JavaScript">
  function konfirmasi()
  {
  tanya = confirm("Anda Yakin Akan Menghapus Data Ini ?");
  if (tanya == true) return true;
  else return false;
  }
</script>


    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <!-- scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
	<script src="/assets/js/ace-extra.min.js"></script>

	<script src="/assets/js/jquery-2.1.4.min.js"></script>
	<script src="/assets/js/jquery-ui.min.js"></script>

@endsection