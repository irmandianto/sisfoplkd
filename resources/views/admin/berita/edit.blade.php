@extends('layouts.admin')
@section('title', 'edit fakultas')
@section('nav', 'edit fakultas')
@section('css')
<!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- page specific plugin styles -->

  <!-- text fonts -->
  <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />

  <!-- styles -->
  <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

  <!--[if lte IE 9]>
    <link rel="stylesheet" href="/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
  <![endif]-->
  <link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
  <link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
@endsection
@section('content')
@include('flash-message')
<div class="col-xs-12 col-sm-12">
  <div class="col-xs-12 col-sm-12 pricing-box">
    <div class="widget-box widget-color-blue">
      <div class="widget-header">
        <h5 class="widget-title bigger lighter">Fakultas</h5>
      </div>

      <div class="widget-body">
        <div class="widget-main">
                         
        <form class="form-horizontal" role="form" action="/home/fakultas/{{ $edit->id }}" method="POST">
          @csrf
          {{method_field('put')}}
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nama Fakultas</label>

            <div class="col-sm-9">
              <input type="text" placeholder="nama fakultas" name="nama_fakultas" value="{{ $edit->nama_fakultas }}" required class="col-xs-10 col-sm-5" />
            </div>
          </div>

          <div class="clearfix form-actions">
            <div class="col-md-offset-10 col-md-9">
              <button class="btn btn-info" type="submit">
                <i class="ace-icon fa fa-check bigger-110"></i>
                Update
              </button>
            </div>
          </div>
        </form>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')

<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
<script type="text/javascript">
  if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<!-- <script>
  $(document).ready(function() {
      $('#dataTables-example').DataTable({
          responsive: true
      });
  });
</script> -->
<script src="/assets/js/jquery-ui.min.js"></script>

<script src="/assets/js/ace-elements.min.js"></script>
    <script src="/ace/assets/js/ace.min.js"></script>
<script src="/assets/js/ace-extra.min.js"></script>
@endsection