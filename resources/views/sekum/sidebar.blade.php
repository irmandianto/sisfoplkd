<li class="active">
	<a href="/home">
		<i class="menu-icon fa fa-tachometer"></i>
		<span class="menu-text"> Dashboard </span>
	</a>

	<b class="arrow"></b>
</li>

<li class="">
	<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-users"></i>
		<span class="menu-text">
			Management User
		</span>

		<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">

		<li class="">
			<a href="/home/sekum/dosen">
				<i class="menu-icon fa fa-caret-right"></i>
				Dosen
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/sekum/instruktur">
				<i class="menu-icon fa fa-caret-right"></i>
				Instruktur
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="/home/sekum/peserta">
				<i class="menu-icon fa fa-caret-right"></i>
				Peserta
			</a>

			<b class="arrow"></b>
		</li>

	</ul>
</li>

