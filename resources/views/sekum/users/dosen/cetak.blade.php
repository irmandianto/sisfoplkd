<!DOCTYPE html>
<html>
<head>
  <title>cetak jadwal kegiatan</title>
  <style type="text/css">
    table, td, th {    
    border-collapse: collapse;
    border: 2px solid #000000;
    width: 100%;    
    text-align: left;
  }

  img {
    border-radius: 50%;
  }
  </style>
</head>
<body>
  <center>
    <h2>Data Dosen</h2>
    <hr>
    <br> <br> 
  </center>
  <br>
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nip</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
        </tr>
    </thead>
    <body>
      @foreach ($dosens as $key => $value)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $value->nip }}</td>
          <td>{{ $value->nama }}</td>
          <td>{{ $value->jenis_kelamin }}</td>
          <td>{{ $value->alamat }}</td>
        </tr>
      @endforeach 
    </body>
  </table>
  
</body>
</html>
