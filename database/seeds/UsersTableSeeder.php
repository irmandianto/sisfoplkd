<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #admin
        $admin                = new User();
	    $admin->nip           = '11111';
	    $admin->nama          = 'om admin';
	    $admin->jenis_kelamin = 'laki-laki';
	    $admin->role		  = 0;
	    $admin->email         = 'admin@gmail.com';
	    $admin->password      =  bcrypt('admin123');
	    $admin->save();
    }
}
