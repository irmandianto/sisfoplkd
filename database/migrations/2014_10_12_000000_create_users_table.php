<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nim', 20)->nullable();
            $table->string('nip', 20)->nullable();
            $table->string('nama', 90);
            $table->string('jenis_kelamin', 20)->nullable();
            $table->string('alamat', 30)->nullable();
            $table->string('foto')->nullable();
            $table->integer('jurusan_id')->nullable();
            $table->integer('progstudi_id')->nullable();
            $table->integer('role');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
