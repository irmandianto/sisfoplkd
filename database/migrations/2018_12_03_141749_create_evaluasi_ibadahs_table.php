<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluasiIbadahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluasi_ibadahs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id');
            $table->string('nama', 30);
            $table->string('minggu_ke', 5);
            $table->integer('sholat_berjamaah')->nullable();
            $table->integer('sholat_dhuha')->nullable();
            $table->integer('tilawah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluasi_ibadahs');
    }
}
