<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    public function dosen()
    {
        return $this->belongsTo('App\User', 'dosen_id');
    }
}
