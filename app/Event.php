<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function kategori()
    {
        return $this->belongsTo('App\kategoriEvent', 'cat_event_id');
    }
}
