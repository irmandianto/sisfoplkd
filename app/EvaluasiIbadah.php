<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluasiIbadah extends Model
{
    public function peserta()
    {
        return $this->belongsTo('App\User', 'peserta_id');
    }
}
