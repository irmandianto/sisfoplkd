<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class SekumProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = User::where('role', 4)->first();
        #dd($profile);
        return view('sekum.profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function ubahpassword(Request $request)
    {
        $this->validate($request, [
            'password'   => 'required|min:6',
        ]);

        $ubahpassword           = User::find(Auth()->user()->id);
        $ubahpassword->password = bcrypt($request->get('password'));
        $ubahpassword->save();

        return redirect()->back()->with('success', 'password berhasil diubah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = User::find($id);
        return view('sekum.profile.edit' ,compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update                = User::find($id);
        $update->nip           = $request->get('nip');
        $update->nama          = $request->get('nama');
        $update->jenis_kelamin = $request->get('jenis_kelamin');
        $update->alamat        = $request->get('alamat');
        $update->email         = $request->get('email');
        $update->update();
        return redirect('/home/profile/sekum')->with('success', 'Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
