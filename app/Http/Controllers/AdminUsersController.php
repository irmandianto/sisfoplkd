<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDosen()
    {
        $dosens = User::where('role', 2)->get();
       # dd($dosens);
        return view('admin.users.dosen.index', compact('dosens'));
    }

    public function indexInstruktur()
    {
        $instrukturs = User::where('role', 1)->get();
        #dd($instrukturs);
        return view('admin.users.instruktur.index', compact('instrukturs'));
    }

    public function indexPeserta()
    {
        $pesertas = User::where('role', 3)->get();
        #dd($pesertas);
        return view('admin.users.peserta.index', compact('pesertas'));
    }

    public function indexSekum()
    {
        $sekums = User::where('role', 4)->get();
        #dd($pesertas);
        return view('admin.users.sekum.index', compact('sekums'));
    }

    public function indexSekumldf()
    {
        $sekumsldf = User::where('role', 5)->get();
        #dd($pesertas);
        return view('admin.users.sekumldf.index', compact('sekumsldf'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDosen(Request $request)
    {
        $save           = new User();
        $save->nama     = $request->get('nama');
        $save->email    = $request->get('email');
        $save->password = bcrypt($request->get('password'));
        $save->role     = 2;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
    }

    public function storeSekum(Request $request)
    {
        $save           = new User();
        $save->nama     = $request->get('nama');
        $save->email    = $request->get('email');
        $save->password = bcrypt($request->get('password'));
        $save->role     = 4;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
    }

    public function storeSekumldf(Request $request)
    {
        $save           = new User();
        $save->nama     = $request->get('nama');
        $save->email    = $request->get('email');
        $save->password = bcrypt($request->get('password'));
        $save->role     = 5;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editDosen($id)
    {
        $edit = User::find($id);
        
        return view('admin.users.dosen.edit', compact('edit'));
    }

    public function editInstruktur($id)
    {
        $edit = User::find($id);
        
        return view('admin.users.instruktur.edit', compact('edit'));
    }

    public function editPeserta($id)
    {
        $edit = User::find($id);
        
        return view('admin.users.peserta.edit', compact('edit'));
    }

    public function editSekum($id)
    {
        $edit = User::find($id);
        
        return view('admin.users.sekum.edit', compact('edit'));
    }

    public function editSekumldf($id)
    {
        $edit = User::find($id);
        
        return view('admin.users.sekumldf.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDosen(Request $request, $id)
    {
        $update           = User::find($id);
        $update->password = bcrypt($request->get('password'));
        $update->update();

        return redirect('/home/dosen')->with('success', 'Berhasil diupdate');
    }

    public function updateInstruktur(Request $request, $id)
    {
        $update           = User::find($id);
        $update->password = bcrypt($request->get('password'));
        $update->update();

        return redirect('/home/instruktur')->with('success', 'Berhasil diupdate');
    }
    
    public function updatePeserta(Request $request, $id)
    {
        $update           = User::find($id);
        $update->password = bcrypt($request->get('password'));
        $update->update();

        return redirect('/home/peserta')->with('success', 'Berhasil diupdate');
    }

    public function updateSekum(Request $request, $id)
    {
        $update           = User::find($id);
        $update->password = bcrypt($request->get('password'));
        $update->update();

        return redirect('/home/sekum')->with('success', 'Berhasil diupdate');
    }

    public function updateSekumldf(Request $request, $id)
    {
        $update           = User::find($id);
        $update->password = bcrypt($request->get('password'));
        $update->update();

        return redirect('/home/sekumldf')->with('success', 'Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDosen($id)
    {
        //
    }
}
