<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvaluasiIbadah;
use App\JadwalDosen;
use PDF;
class DosenEvaluasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluasis = EvaluasiIbadah::all();
        return view('dosen.evaluasi.index', compact('evaluasis'));
    }

    public function jadwal()
    {
        $jadwals = JadwalDosen::all();
        return view('dosen.jadwaldosen.index', compact('jadwals'));
    }

    public function cetakjadwal()
    {
        
        $jadwals = JadwalDosen::get();
        $pdf     = PDF::loadView('dosen.jadwaldosen.cetak', compact('jadwals'));
        
        return $pdf->stream();
    }

}
