<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JadwalDosen;
class AdminJadwalDosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwals = JadwalDosen::all();
        return view('admin.jadwaldosen.index', compact('jadwals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save           = new JadwalDosen();
        $save->hari     = $request->get('hari');
        $save->waktu    = $request->get('waktu');
        $save->isi      = $request->get('isi');
        $save->admin_id = Auth()->user()->id;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = JadwalDosen::find($id);
        
        return view('admin.jadwaldosen.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update           = JadwalDosen::find($id);
        $update->hari     = $request->get('hari');
        $update->waktu    = $request->get('waktu');
        $update->isi      = $request->get('isi');
        $update->update();
        return redirect('/home/jadwal-dosen')->with('success', 'Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = JadwalDosen::find($id);
        $delete->delete();

        return redirect()->back()->with('warning', 'Berhasil dihapus');
    }
}
