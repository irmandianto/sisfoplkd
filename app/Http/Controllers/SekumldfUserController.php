<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PDF;
class SekumldfUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sekums = User::where('role', 4)->get();
       # dd($sekums);
        return view('sekumldf.users.sekum.index', compact('sekums'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save           = new User();
        $save->nama     = $request->get('nama');
        $save->email    = $request->get('email');
        $save->password = bcrypt($request->get('password'));
        $save->role     = 4;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
    }

    public function cetak()
    {
        
        $sekum = User::where('role', 4)->get();
        $pdf     = PDF::loadView('sekumldf.users.sekum.cetak', compact('sekum'));
        
        return $pdf->stream();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = User::find($id);

        return view('sekumldf.users.sekum.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update           = User::find($id);
        $update->password = bcrypt($request->get('password'));
        $update->update();

        return redirect('/home/sekumldf/sekum')->with('success', 'berhasil diubah'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
