<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Auth\Events\Registered;

class LoginController extends Controller
{
    

    public function showLoginForm()
    {
        return view('auth.login');

    }

    public function login(Request $request)
    {
       // Validation the form data
        $this->validate($request, [
            'password'     => 'required',
            'email'        => 'required',
            'role'         => 'required'
        ]);
        
        switch ($request->role) {

            case 0:
                
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => $request->role], $request->remember))
                {
                    return redirect('/home');
                }

                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');

            break;
            case 1:

                if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => $request->role], $request->remember))
                {
                    return redirect('/home');
                }

                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');
            break;
            case 2:
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => $request->role], $request->remember))
                {
                    return redirect('/home');
                }

                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');
            break;
            case 3:
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => $request->role], $request->remember))
                {
                    return redirect('/home');
                }

                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');
            break;
            case 4:
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => $request->role], $request->remember))
                {
                    return redirect('/home');
                }

                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');
            break;
            case 5:
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => $request->role], $request->remember))
                {
                    return redirect('/home');
                }

                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');
            break;
                
            default:
                return redirect()->back()
                       ->withInput($request->only('email', 'remember'))
                       ->with('warning','pastikan login dengan data yang benar.');    
            break;
        }
    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect('/');
    }
}
