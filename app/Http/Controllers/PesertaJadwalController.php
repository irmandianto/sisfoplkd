<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use PDF;
class PesertaJadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwals = Jadwal::all();
        #dd($jadwals);
        return view('peserta.jadwal.index', compact('jadwals'));
    }


    public function cetak($id)
    {
        
        $jadwal = Jadwal::find($id); 
        $pdf     = PDF::loadView('peserta.jadwal.cetak', compact('jadwal'))->setPaper('a4', 'landscape');
        
        return $pdf->stream();
    }
}
