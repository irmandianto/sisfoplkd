<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materi;
use App\User;
use File;
class AdminMateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materi = Materi::all();
/*        $dosens      = User::where('role', 2)->get();
        $pesertas    = User::where('role', 3)->get();
        $instrukturs = User::where('role', 1)->get();*/
        return view('admin.materi.index', compact('materi', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file'   => 'required|mimes:docx,pdf,doc|max:5000',
        ]);

        $file                   = $request->file('file');
        $ext                    = $file->getClientOriginalExtension();
        $newName                = rand(100000,1001238912).".".$ext;
        $file->move('materi',$newName);

        $save           = new Materi();
/*        $save->dosen_id         = $request->get('dosen_id');
        $save->peserta_id       = $request->get('peserta_id');
        $save->instruktur_id    = $request->get('instruktur_id');*/
        $save->role_id  = $request->get('role_id');
        $save->file     = $newName;
        $save->admin_id = $request->user()->id;
        $save->save();

        if(!$save){
            return redirect()->back()->with('warning', 'Gagal disimpan');
        }
        return redirect()->back()->with('success', 'Berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Materi::findorFail($id);
        $delete->delete();
        File::delete(public_path('materi/'. $delete->file));

        if(!$delete){
            return redirect()->back()->with('warning', 'Gagal dihapus');
        }
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
}
