<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Event;

class TampilanDepanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('tampildepan.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        /*$events  = Event::paginate(5);
        $beritas = Berita::paginate(5);

        return view('tampildepan.home', compact('events', 'beritas'));*/
        return view('tampildepan.profile');
    }

    public function berita()
    {
        $beritas = Berita::all();
        #dd($beritas);
        return view('tampildepan.berita', compact('beritas'));
    }

    public function event()
    {
        $events  = Event::paginate(20);
        #$beritas = Berita::paginate(5);
        #dd($beritas);
        return view('tampildepan.event', compact('events'));
    }

    public function beritadetail($id)
    {
        $berita = Berita::find($id);
        #dd($berita);
        return view('tampildepan.beritadetail', compact('berita'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
