<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PDF;
class SekumUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexdosen()
    {
        $dosens = User::where('role', 2)->get();
        return view('sekum.users.dosen.index', compact('dosens'));
    }

    public function cetakdosen()
    {
        
        $dosens = User::where('role', 2)->get();
        
        $pdf    = PDF::loadView('sekum.users.dosen.cetak', compact('dosens'));
        
        return $pdf->stream();
    }

    public function indexpeserta()
    {
        $pesertas = User::where('role', 3)->get();
        return view('sekum.users.peserta.index', compact('pesertas'));
    }

    public function cetakpeserta()
    {
        
        $pesertas = User::where('role', 3)->get();
        $pdf      = PDF::loadView('sekum.users.peserta.cetak', compact('pesertas'));
        
        return $pdf->stream();
    }

    public function indexinstruktur()
    {
        $instrukturs = User::where('role', 1)->get();
        return view('sekum.users.instruktur.index', compact('instrukturs'));
    }

    public function cetakinstruktur()
    {
        
        $instruktur = User::where('role', 1)->get();
        $pdf        = PDF::loadView('sekum.users.instruktur.cetak', compact('instruktur'));
        
        return $pdf->stream();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
