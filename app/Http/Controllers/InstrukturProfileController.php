<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Jadwal;
use File;
use PDF;
class InstrukturProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = User::where('role', 1)->first();
        #dd($profile);
        return view('instruktur.profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jadwal()
    {
        $jadwals = Jadwal::all();
        return view('instruktur.jadwal.index', compact('jadwals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function ubahpassword(Request $request)
    {
        $this->validate($request, [
            'password'   => 'required|min:6',
        ]);

        $ubahpassword           = User::find(Auth()->user()->id);
        $ubahpassword->password = bcrypt($request->get('password'));
        $ubahpassword->save();

        return redirect()->back()->with('success', 'password berhasil diubah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = User::find($id);
        return view('instruktur.profile.edit' ,compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update                = User::find($id);
        $update->nama          = $request->get('nama');
        $update->jenis_kelamin = $request->get('jenis_kelamin');
        $update->alamat        = $request->get('alamat');
        $update->email         = $request->get('email');
        $update->update();
        return redirect('/home/profile/instruktur')->with('success', 'Berhasil diupdate');
    }

    public function updatefoto(Request $request, $id)
    {

        $this->validate($request, [
            'foto'   => 'required|mimes:jpeg,jpg,png|max:2000',
        ]);
        #get foto lama
        $fotolama     = User::find(auth()->user()->id);
        #input foto baru
        $file         = $request->file('foto');
        $ext          = $file->getClientOriginalExtension();
        $newName      = rand(100000,1001238912).".".$ext;
        $file->move('foto',$newName);

        $update       = User::find($id);
        $update->foto = $newName;
        $update->update();

        File::delete(public_path('foto/'. $fotolama->foto));

        if(!$update){
            return redirect()->back()->with('warning', 'Gagal disimpan');
        }
        return redirect()->back()->with('success', 'Foto Profile Berhasil Diubah');
    }
    
    public function cetak($id)
    {
        
        $peserta = User::find($id); 
        $pdf     = PDF::loadView('instruktur.profile.cetak', compact('peserta'))->setPaper('a6', 'portrait');
        
        return $pdf->stream();
    }

    public function cetakjadwal($id)
    {
        $jadwal  = Jadwal::find($id); 
        $pdf     = PDF::loadView('instruktur.jadwal.cetak', compact('jadwal'))->setPaper('a4', 'portrait');
        
        return $pdf->stream();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
