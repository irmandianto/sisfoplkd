<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProgramStudi;
use App\Jurusan;
use App\User;
use File;
use PDF;
class PesertaProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile      = User::where('role', 3)->first();

        #dd($profile);
        return view('peserta.profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function ubahpassword(Request $request)
    {
        $this->validate($request, [
            'password'   => 'required|min:6',
        ]);

        $ubahpassword           = User::find(Auth()->user()->id);
        $ubahpassword->password = bcrypt($request->get('password'));
        $ubahpassword->save();

        return redirect()->back()->with('success', 'password berhasil diubah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit         = User::find($id);
        #dd($edit->jurusan_id);
        $jurusans     = Jurusan::all();
        
        $programstudi = ProgramStudi::all();
        
        return view('peserta.profile.edit' ,compact('edit', 'jurusans', 'programstudi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update                  = User::find($id);
        $update->nama            = $request->get('nama');
        $update->jenis_kelamin   = $request->get('jenis_kelamin');
        $update->alamat          = $request->get('alamat');
        $update->jurusan_id      = $request->get('jurusan_id');
        $update->progstudi_id    = $request->get('progstudi_id');
        $update->email           = $request->get('email');
        $update->update();
        return redirect('/home/profile/peserta')->with('success', 'Berhasil diupdate');
    }

    public function updatefoto(Request $request, $id)
    {

        $this->validate($request, [
            'foto'   => 'required|mimes:jpeg,jpg,png|max:2000',
        ]);
        #get foto lama
        $fotolama     = User::find(auth()->user()->id);
        #input foto baru
        $file         = $request->file('foto');
        $ext          = $file->getClientOriginalExtension();
        $newName      = rand(100000,1001238912).".".$ext;
        $file->move('foto',$newName);

        $update       = User::find($id);
        $update->foto = $newName;
        $update->update();

        File::delete(public_path('foto/'. $fotolama->foto));

        if(!$update){
            return redirect()->back()->with('warning', 'Gagal disimpan');
        }
        return redirect()->back()->with('success', 'Foto Profile Berhasil Diubah');
    }
    public function cetak($id)
    {
        
        $peserta = User::find($id); 
        $pdf     = PDF::loadView('peserta.profile.cetak', compact('peserta'))->setPaper('a6', 'portrait');
        
        return $pdf->stream();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
