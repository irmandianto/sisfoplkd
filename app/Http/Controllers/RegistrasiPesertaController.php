<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegistrasiPesertaController extends Controller
{
    public function registrasiPeserta(Request $minta)
    {
      $this->validate($minta, [
            'nim'           => 'required|unique:users',
            'nama'          => 'required',
            'jenis_kelamin' => 'required',
            'email'         => 'required|unique:users',
            'password'      => 'required',
        ]);

      $save                = new User();
      $save->nim           = $minta->get('nim');
      $save->nama          = $minta->get('nama');
      $save->jenis_kelamin = $minta->get('jenis_kelamin');
      $save->email         = $minta->get('email');
      $save->password      = bcrypt($minta->get('password'));
      $save->role          = $minta->get('role');
      $save->save();

      return redirect()->back()
                       ->with('success','berhasil registrasi.');
    }

    public function registrasiDosen(Request $minta)
    {
      $this->validate($minta, [
            'nip'           => 'required|unique:users',
            'nama'          => 'required|max:30',
            'jenis_kelamin' => 'required',
            'email'         => 'required|unique:users',
            'password'      => 'required',
        ]);

      $save                = new User();
      $save->nip           = $minta->get('nip');
      $save->nama          = $minta->get('nama');
      $save->jenis_kelamin = $minta->get('jenis_kelamin');
      $save->email         = $minta->get('email');
      $save->password      = bcrypt($minta->get('password'));
      $save->role          = 2;
      $save->save();

      return redirect()->back()
                       ->with('success','berhasil registrasi.');
    }
}
