<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvaluasiIbadah;
use App\Event;
class PesertaEvaluasiIbadahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluasis = EvaluasiIbadah::all();
        return view('peserta.evaluasiibadah.index', compact('evaluasis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function event()
    {
        $events = Event::all();
        
        return view('peserta.event.index', compact('events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate($request, [
            'nama'   => 'required|max:30',
        ]);*/
        $save                   = new EvaluasiIbadah();
        $save->nama             = Auth()->user()->nama;
        
        $save->minggu_ke        = $request->get('minggu_ke');
        $save->sholat_berjamaah = $request->get('sholat_berjamaah');
        $save->sholat_dhuha     = $request->get('sholat_dhuha');
        $save->tilawah          = $request->get('tilawah');
        $save->peserta_id       = Auth()->user()->id;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $edit = Jadwal::find($id);
        
        return view('admin.jadwal.edit', compact('edit'));
    }

    public function update(Request $request, $id)
    {
        $update           = Jadwal::find($id);
        $update->hari     = $request->get('hari');
        $update->waktu    = $request->get('waktu');
        $update->isi      = $request->get('isi');
        $update->update();
        return redirect('/home/jadwal')->with('success', 'Berhasil diupdate');
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = EvaluasiIbadah::find($id);
        $delete->delete();

        return redirect()->back()->with('warning', 'Berhasil dihapus');
    }
}
