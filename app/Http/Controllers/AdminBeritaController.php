<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use File;
class AdminBeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $beritas = Berita::all();

        return view('admin.berita.index', compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'foto' => 'required|mimes:jpeg,jpg,png,mp4,gif|max:8000',
        ]);

        $file    = $request->file('foto');
        $ext     = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('berita',$newName);

        $save             = new Berita();
        $save->judul      = $request->get('judul');
        $save->isi        = $request->get('isi');
        $save->jenis_file = $request->get('jenis_file');
        $save->foto       = $newName;
        $save->admin_id   = $request->user()->id;
        $save->save();

        if(!$save){
            return redirect()->back()->with('warning', 'Gagal disimpan');
        }
        return redirect()->back()->with('success', 'Berhasil disimpan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Fakultas::find($id);
        return view('admin.fakultas.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$update                = Fakultas::find($id);
        $update->nama_fakultas = $request->get('nama_fakultas');
        $update->update();
        return redirect('/home/fakultas')->with('success', 'Berhasil');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Berita::findorFail($id);
        $delete->delete();
        File::delete(public_path('berita/'. $delete->foto));

        if(!$delete){
            return redirect()->back()->with('warning', 'Gagal dihapus');
        }
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
}
