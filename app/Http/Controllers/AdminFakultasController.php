<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fakultas;
use DataTables;

class AdminFakultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $fakultas = Fakultas::all();
        return view('admin.fakultas.index', compact('fakultas'));
    }

    function getdata()
    {
        $fasilitas = Fakultas::select(['nama_fakultas']);
        return Datatables::of($fasilitas)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Fakultas();
        $save->nama_fakultas = $request->nama_fakultas;
        $save->admin_id      = Auth()->user()->id;
        $save->save();

        return redirect()->back()->with('success', 'Berhasil disimpan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Fakultas::find($id);
        return view('admin.fakultas.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update                = Fakultas::find($id);
        $update->nama_fakultas = $request->get('nama_fakultas');
        $update->update();
        return redirect('/home/fakultas')->with('success', 'Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Fakultas::find($id);
        $delete->delete();

        return redirect()->back()->with('warning', 'Berhasil dihapus');
    }
}
