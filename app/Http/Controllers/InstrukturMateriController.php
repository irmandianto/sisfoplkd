<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materi;
use App\User;
use PDF;
class InstrukturMateriController extends Controller
{
    public function index()
    {
        $materis = Materi::where('role_id', 1)->get();
        #dd($materi);
        return view('instruktur.materi.index', compact('materis'));
    }

    public function peserta()
    {
        $pesertas = User::where('role', 3)->get();
        
        #dd($pesertas);
        return view('instruktur.peserta.index', compact('pesertas'));
    }

    public function cetak()
    {
        
        $pesertas = User::where('role', 3)->get(); 
        $pdf      = PDF::loadView('instruktur.peserta.cetak', compact('pesertas'));
        
        return $pdf->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download($file)
    {
        $file_path = public_path('materi/'.$file);
        return response()->download($file_path);
    }
}
