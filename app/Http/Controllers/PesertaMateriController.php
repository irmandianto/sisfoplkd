<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materi;
class PesertaMateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materis = Materi::where('role_id', 3)->get();
        #dd($materi);
        return view('peserta.materi.index', compact('materis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download($file)
    {
        $file_path = public_path('materi/'.$file);
        return response()->download($file_path);
    }

}
