<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AuthAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() &&  Auth::user()->role == 0) {
            return $next($request);
     }

        return response(view('error.403'));
    }
}
