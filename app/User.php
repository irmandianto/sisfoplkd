<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\ProgramStudi;
use App\Jurusan;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function materis()
    {
        return $this->hasMany('App\Materi');
    }

    public function programstudi()
    {
        return $this->belongsTo('App\ProgramStudi', 'progstudi_id');
    }

    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }
}
